declare module 'z-trans' {

    namespace trans {
        function addData(locale: string, transData: any): void
        function addDomainData(locale: string, domain: string, data: any): void
        function domain(domain: string): void
        function trans(key: string, data?: any, domain?: string, locale?: string): string
    }

    export default trans;
    export { trans, domain } from 'z-trans';
}
