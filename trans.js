'use strict';
function Trans() {
    this.defaultLocale = null;
    this.globalDomain = null;
    this.locale = null;
    this.transData = {};
}

Trans.prototype.addData = function(locale, transData) {
    this.transData[locale] = transData;
};

Trans.prototype.addDomainData = function(locale, domain, data) {
    if (this.transData[locale] === undefined) {
        this.transData[locale] = {};
    }
    this.transData[locale][domain] = data;
};

Trans.prototype.domain = function(domain) {
    this.globalDomain = domain;
};

Trans.prototype.trans = function(key, data, domain, locale) {
    if (locale === undefined) {
        locale = this.locale;
    }

    if (domain === undefined) {
        domain = this.globalDomain;
    }

    if (
        this.transData[locale] === undefined ||
        this.transData[locale][domain] === undefined ||
        this.transData[locale][domain][key] === undefined
    ) {
        if (
            this.transData[this.defaultLocale] !== undefined &&
            this.transData[this.defaultLocale][domain] !== undefined &&
            this.transData[this.defaultLocale][domain][key] !== undefined
        ) {
            locale = this.defaultLocale;
        } else {
            return key;
        }
    }

    if (typeof data !== 'object') {
        data = {};
    }

    var transString = this.transData[locale][domain][key];
    for (var index in data) {
        transString = transString.replace(new RegExp(index, 'g'), data[index]);
    }

    return transString;
};

var _trans = new Trans();
module.exports = _trans;
module.exports.trans = Trans.prototype.trans.bind(_trans);
module.exports.domain = Trans.prototype.domain.bind(_trans);
